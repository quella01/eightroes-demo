package com.ssrs.demo;

import com.ssrs.framework.extend.plugin.AbstractPlugin;
import com.ssrs.framework.extend.plugin.PluginException;

/**
 * @author ssrs
 */
public class DemoPlugin extends AbstractPlugin {
    @Override
    public void start() throws PluginException {

    }

    @Override
    public void stop() throws PluginException {

    }
}
